package scalatrix

import Util._
import Util.Transform._
import scala.util.Random
import scala.collection.SortedSet

/**
 * A Block is a (x,y) matrix of arbitrary size.
 * It contains a collection of (x,y) tuples: each tuple represents a "taken" spot inside the matrix.
 * @param origin the origin (x,y) to rotate around
 * @param fields the collection of "taken" (x,y) spots inside this matrix
 */
case class Block(origin: Tile[Float], fields: SortedSet[Tile[Int]]) {
  import scalatrix.TileOrdering._

  /**
   * the start (x,y) of the matrix
   */
  lazy val min: Tile[Int] = Tile(fields.minBy(_.x).x, fields.minBy(_.y).y)

  /**
   * the end (x,y) of the matrix
   */
  lazy val max: Tile[Int] = Tile(fields.maxBy(_.x).x, fields.maxBy(_.y).y)

  def transform(r: Transform): Block = {
    r match {
      case Transform.RotateLeft => rotateLeft()
      case Transform.RotateRight => rotateRight()
      case Transform.MoveUp => translate(-1 <> 0)
      case Transform.MoveDown => translate(1 <> 0)
      case Transform.MoveLeft => translate(0 <>- 1)
      case Transform.MoveRight => translate(0 <> 1)
    }
  }

  def intersects(other: Block): Boolean = {
    !(this.fields & other.fields).isEmpty
  }


  /**
   * Merge this block other block. Rotation origin stays from this block.
   * @param other the other block to merge with
   * @return the new merged block
   */
  def <++(other: Block): Block =
    Block(origin, fields ++ other.fields)

  /**
   * Cut-out this block using the other block. Rotation origin stays from this block.
   * @param other the other block to cut-out with
   * @return the new cut-out block
   */
  def <--(other: Block): Block =
    Block(origin, fields -- other.fields)


  // just remove rows; don't change other parameters
  private def squashVertical(newPos: (Int, Int) => Int): Block = {
    val fullRows = SortedSet.empty[Int] ++ fields.groupBy(_.x).filter(_._2.size == COLS).keySet
    if (!fullRows.isEmpty)
      Block(origin, fields.collect {
        case t@Tile(x, y) if (!fullRows.contains(x)) => Tile(newPos(x, fullRows.from(x).size), y)
      })
    else
      this
  }

  /**
   * Remove full rows while shifting the remaining rows upwards.
   */
  def squashUp(): Block = {
    squashVertical(_ - _)
  }

  /**
   * Remove full rows while shifting the remaining rows downwards.
   */
  def squashDown(): Block = {
    squashVertical(_ + _)
  }

  // just remove cols; don't change other parameters
  private def squashHorizontal(newPos: (Int, Int) => Int): Block = {
    val fullCols = SortedSet.empty[Int] ++ fields.groupBy(_.y).filter(_._2.size == ROWS).keySet
    if (!fullCols.isEmpty)
      Block(origin, fields.collect {
        case t@Tile(x, y) if (!fullCols.contains(y)) => Tile(x, newPos(y, fullCols.from(y).size))
      })
    else
      this
  }

  /**
   * Remove full cols while shifting the remaining cols left.
   */
  def squashLeft(): Block = {
    squashHorizontal(_ - _)
  }

  /**
   * Remove full cols while shifting the remaining cols right.
   */
  def squashRight(): Block = {
    squashHorizontal(_ + _)
  }


  def rotateRight(): Block = {
    Block(origin, fields.map(_ rotateLeft origin))
  }

  def rotateLeft(): Block = {
    Block(origin, fields.map(_ rotateRight origin))
  }

  def translate(t: Tile[Int]): Block = Block(origin + Tile(t.x.toFloat, t.y.toFloat),
    fields.map(_ + t))

  override def toString: String = {
    var out = ""

    for (i <- min.x to max.x; j <- min.y to max.y) {
      if (fields.contains(Tile(i, j)))
        out += "X"
      else
        out += "-"

      out += " "
      if (j == max.y) out += "\n"
    }

    out
  }
}

object Block {
  import scalatrix.TileOrdering._

  val ShapeI: Block = Block(0f <> 1.5f, SortedSet(0 <> 0, 0 <> 1, 0 <> 2, 0 <> 3))
  val ShapeO: Block = Block(0.5f <> 0.5f, SortedSet(0 <> 0, 0 <> 1, 1 <> 0, 1 <> 1))
  val ShapeT: Block = Block(1f <> 1f, SortedSet(0 <> 1, 1 <> 0, 1 <> 1, 1 <> 2))
  val ShapeJ: Block = Block(1f <> 1f, SortedSet(0 <> 0, 1 <> 0, 1 <> 1, 1 <> 2))
  val ShapeL: Block = Block(1f <> 1f, SortedSet(0 <> 2, 1 <> 0, 1 <> 1, 1 <> 2))
  val ShapeZ: Block = Block(1f <> 1f, SortedSet(0 <> 0, 0 <> 1, 1 <> 1, 1 <> 2))
  val ShapeS: Block = Block(1f <> 1f, SortedSet(1 <> 0, 1 <> 1, 0 <> 1, 0 <> 2))
  val ShapeNone: Block = Block(0f <> 0f, SortedSet.empty[Tile[Int]])

  private val shapes: List[Block] =
    List(ShapeI, ShapeO, ShapeT, ShapeL, ShapeJ, ShapeZ, ShapeS)

  def apply(): Block = {
    shapes(Random.nextInt(shapes.length))
  }

  def apply(tile: Tile[Int]): Block = {
    Block(Tile(tile.x.toFloat, tile.y.toFloat), SortedSet(tile))
  }

  def apply(tiles: SortedSet[Tile[Int]], origin: Tile[Float] = 0f <> 0f): Block = {
    Block(origin, tiles)
  }
}