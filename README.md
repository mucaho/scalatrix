# README #

This is an example Tetris implementation that somewhat complies to the ["official" Tetris Guideline](http://tetris.wikia.com/wiki/Tetris_Guideline).   
It was created as a demo project to test the capabilities and interoperability between scala, akka and scalafx.

Arrow keys are used to control the currently falling piece. Rotate with `UP_ARROW`, let it fall faster with `DOWN_ARROW`.

Download & run [the jar](https://bitbucket.org/mucaho/scalatrix/downloads/Scalatrix.jar).

Alternatively clone the project and compile & run it yourself with `sbt run`.   
Requirements:   
* [OracleJDK8+](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or [OpenJDK8+ compiled with JavaFX](https://wiki.openjdk.java.net/display/OpenJFX/Building+OpenJFX)    
* [sbt ~0.13.15](http://www.scala-sbt.org/download.html)   
* `scala`, `akka` and `scalafx` will be automatically downloaded by sbt