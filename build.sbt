val akka = "com.typesafe.akka" %% "akka-actor" % "2.5.0"
val scalafx = "org.scalafx" %% "scalafx" % "8.0.102-R11"


lazy val root = (project in file("."))
  .settings(
    name := "scalatrix",
    scalaVersion := "2.12.2",
    javacOptions ++= Seq("-source", "1.8", "-target", "1.8"),
    scalacOptions += "-target:jvm-1.8",
    mainClass in (Compile, run) := Some("scalatrix.Main"),
    libraryDependencies ++= Seq(akka, scalafx)
  )
